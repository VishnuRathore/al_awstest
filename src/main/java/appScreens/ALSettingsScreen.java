package appScreens;

import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidElement;

public class ALSettingsScreen {
	
	public WebElement settings_security,security_description_ftv,
	settings_lock_delay,lock_delay_time_ftv,
	settings_data_saving_mode,
	settings_notification,notification_status_description_ftv,
	settings_wallpaper,
	settings_weather,weather_description_ftv,
	settings_terms_and_condition;
	
	public WebElement lock_delay_none,lock_delay_30_seconds,lock_delay_1_minute,lock_delay_2_minute,lock_delay_3_minutes,lock_delay_4_minutes,lock_delay_5_minutes,
	
	security_none_linear_layout,security_pin_linear_layout,ok_button_on_pin;

	}

