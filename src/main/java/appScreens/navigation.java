package appScreens;

import elementHandler.ElementHandler;
import elementHandler.GetElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class navigation {
	
	public static boolean lockscreen(AndroidDriver lockScreen){
	GetElement.ByName("GET STARTED", lockScreen).click();
	  GetElement.ByName("Launch Screen", lockScreen).click();
	  try{
		  if(ElementHandler.isElementPresentByName("OK",lockScreen)){
			  GetElement.ByName("OK", lockScreen).click();
			  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
			  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
			  return true;
		  }
		  else{
			  GetElement.ByName("ALLOW", lockScreen).click();
			  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
			  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
			  return true;}
	  }catch(Exception e){
		  //Hit Button by ID
		  return false;
	  }
	}
}
