package elementHandler;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ElementHandler {
	
	public static boolean isElementPresent(AndroidDriver AppDriver, By by)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			AppDriver.findElement(by);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	

	public static boolean isElementPresentByXpath(AndroidDriver AppDriver,String Xpath)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			AppDriver.findElementByXPath(Xpath);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	public static boolean isElementTappable(AndroidDriver AppDriver, By by)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			AndroidElement Ae=(AndroidElement) AppDriver.findElement(by);
			String clickable=Ae.getAttribute("clickable");
			if(clickable.equalsIgnoreCase("false")){
				return false;
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static String getElementText(AndroidDriver AppDriver, By by)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			AndroidElement Ae=(AndroidElement) AppDriver.findElement(by);
			String Label=Ae.getText();
			return Label;
		}catch(Exception e){
			System.out.println("Unable to locate the Element");
			return null;
		}
	}
	
	public static String getAttribute(AndroidDriver AppDriver,By by,String attribute)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
			AndroidElement Ae=(AndroidElement) AppDriver.findElement(by);
			String att_val=Ae.getAttribute(attribute);
			return att_val;
		}catch(Exception e){
			System.out.println("Unable to locate the Element");
			return null;
		}
	}
	public static AndroidElement get(AndroidDriver AppDriver, By by)
	{
		try{
			AppDriver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
			AndroidElement ae=(AndroidElement) AppDriver.findElement(by);
			return ae;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<AndroidElement> getList(AndroidDriver ae,By by){
		try{
			List<AndroidElement> aeList=ae.findElements(by);
			return aeList;
		}catch(Exception e){
			return null;
		}
	}
	
	public static void loadWait(AndroidDriver myApp, int seconds){
		try {
			Thread.sleep(seconds*1000);
			} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public static boolean isElementPresentByName(String text, AndroidDriver myApp) {
		AndroidElement ae;
		try{
			myApp.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
			ae=(AndroidElement) myApp.findElement(By.xpath("//*[@text='"+text+"']"));
			return true;
		}catch(Exception e){
			//e.printStackTrace();
			return false;
	}
}
}
