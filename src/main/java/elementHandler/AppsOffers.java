package elementHandler;

import java.util.List;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class AppsOffers {

	public static List<AndroidElement> getList(AndroidDriver myApp){
		List<AndroidElement> appOffers;
		appOffers=myApp.findElements(By.id("appName"));
		return appOffers;
	}
}
 