package actionHandler;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import elementHandler.ElementHandler;
import elementHandler.GetElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class UiNavigation {
	public static AndroidElement ae;
	
	public static boolean openNav(AndroidDriver myApp){
		try{
			myApp.tap(1,500,500,200);
			Thread.sleep(100);
			PerformBasicAction.swipeLeftSlidebar(myApp); 
			Thread.sleep(2500);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean closeNav(AndroidDriver myApp){
		try{
			
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean navToOnboardingScreen(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			return true;
			
		}catch(NoSuchElementException e){
			System.out.println("Are you on Permission Screen?");
			return false;
		}
	}
	
	public static boolean navToHomeScreen(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			
			
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();
			
			PerformBasicAction.waitforTime(500);
			myApp.tap(1,500,500,500);
			
			PerformBasicAction.waitforTime(500);
			myApp.tap(1,500,500,500);
			return true;
			
		}catch(NoSuchElementException e){
			System.out.println("Are you on Permission Screen?");
			return false;
		}
	}
	
	public static boolean navToSettingScreen(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();
			myApp.tap(1,500,500,500);
			PerformBasicAction.waitforTime(500);
			myApp.tap(1,500,500,500);	
			ae=GetElement.ByID(myApp,By.id("setting_icon_fa_tv"));
			ae.click();
			return true;
			
		}catch(NoSuchElementException e){
			System.out.println("Are you on Permission Screen?");
			return false;
		}
	}
	
	public static boolean navToSearchScreen(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();
			ae=GetElement.ByID(myApp,By.id("search_icon_fa_tv"));
			ae.click();
			return true;
		}catch(NoSuchElementException e){
			System.out.println("Are you on Permission Screen?");
			return false;
		}
	}
	
	public static boolean navHomeToUSCellularApps(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();
			
			myApp.tap(1,500,500,500);
			PerformBasicAction.waitforTime(500);
			myApp.tap(1,500,500,500);	
			myApp.findElement(By.id("catLogo")).click();
			
			return true;
		}catch(NoSuchElementException e){
			System.out.println("Are you on Permission Screen?");
			return false;
		}
	}
	
	public static boolean navToHomeScreen(AndroidDriver myApp, String menuName){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();		
			myApp.tap(1,500,500,500);
			PerformBasicAction.waitforTime(500);
			myApp.tap(1,500,500,500);	
			myApp.findElement(By.id("catLogo")).click();
			
			return true;
		}catch(NoSuchElementException e){

			return false;
		}
	}
	
	public static boolean navToCoachScreen(AndroidDriver myApp){
		try{
			ae=GetElement.ByName("Accept", myApp);
			ae.click();
			ae=GetElement.ByName("SKIP", myApp);
			ae.click();		
			return true;
		}catch(NoSuchElementException e){

			return false;
		}
	}
	
	  public static void NavigateToMenuByName(AndroidDriver myApp,String menuName){
			try{
				  myApp.findElement(By.className("android.widget.ImageButton")).click();
				  Thread.sleep(1000);
				  if(ElementHandler.isElementPresent(myApp,By.id("nav_drawer_see_more_tv"))){
					  myApp.findElement(By.id("nav_drawer_see_more_tv")).click();
				  }
				  
				  GetElement.ByName(menuName, myApp).click();
			
			}catch(Exception e){
				e.printStackTrace();
			}
		  }

		  public static void NavigateToMenuByIndex(AndroidDriver myApp,int index){
			try{
				  myApp.findElement(By.className("android.widget.ImageButton")).click();
				  Thread.sleep(1000);
				  //Expand See More
				  if(ElementHandler.isElementPresent(myApp,By.id("nav_drawer_see_more_tv"))){
					  myApp.findElement(By.id("nav_drawer_see_more_tv")).click();
				  }
				  myApp.findElementByXPath("//android.widget.ListView/android.widget.LinearLayout[@index='"+index+"']").click();
			
			}catch(Exception e){
				e.printStackTrace();
			}
		  }
		  
}