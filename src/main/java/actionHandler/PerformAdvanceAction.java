package actionHandler;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;

public class PerformAdvanceAction {
	static AndroidDriver driver;

	public static String getActivityName(AndroidDriver myApp){
		String activityName=myApp.currentActivity();
		return activityName;
	}
	
	public static boolean takeScreenshot(AndroidDriver driver,final String name) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       String screenshotDirectory = System.getProperty("appium.screenshots.dir", System.getProperty("java.io.tmpdir", ""));
	       File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	       return screenshot.renameTo(new File(screenshotDirectory, String.format("%s.png", name)));
	   }
}
