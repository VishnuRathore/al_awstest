package actionHandler;

import org.openqa.selenium.By;
import org.testng.Assert;

import elementHandler.ElementHandler;
import elementHandler.GetElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class RedirectionHandler {

	public static boolean EssentialApps(AndroidElement ae,AndroidDriver device,int i){
		try{
			int timeout=0;
			String appTitle=ae.getText();
			System.out.println("Redirection Test for->"+appTitle);
			device.findElement(By.id("app_"+i)).click();
			while(device.currentActivity().equalsIgnoreCase("com.google.android.finsky.activities.MainActivity"))
			{
				Thread.sleep(10000);
				timeout++;
				if(timeout>=6){
					System.out.println("Timeout Error- Unable to Launch Playstore.");
					Assert.fail("Unable to Launch Playstore on Device");
					return false;
				}
				
			}
				Thread.sleep(20000);
				ae=(AndroidElement) device.findElementByXPath("//android.widget.TextView[@index='2']");
				String actualAppName=ae.getText();
				//System.out.println(actualAppName);
				if(actualAppName.matches(appTitle))
				{
					System.out.println("Match");
				}
				//Assert.assertEquals(actualAppName,appTitle);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean forAppName(AndroidElement ae,AndroidDriver device){
		try{
			int timeout=0;
			String appTitle=ae.getText();
			System.out.println("Redirection Test for->"+appTitle);
			ae.click();
			while(device.currentActivity().equalsIgnoreCase("com.google.android.finsky.activities.MainActivity"))
			{
				Thread.sleep(10000);
				timeout++;
				if(timeout>=6){
					System.out.println("Timeout Error- Unable to Launch Playstore.");
					return false;
				}
			}
				Thread.sleep(20000);
//				if(ElementHandler.isElementPresent(device,By.xpath("//android.widget.TextView[@index='2']"))){
//				String actualAppName=device.findElementByXPath("//android.widget.TextView[@index='2']").getText();
//				Assert.assertEquals(actualAppName,appTitle);
	
				device.longPressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(10000);
				if(device.currentActivity().equalsIgnoreCase("com.google.android.finsky.activities.MainActivity")){
					device.longPressKeyCode(AndroidKeyCode.BACK);
					Thread.sleep(10000);
				}
				return true;
//				}else{
//					device.longPressKeyCode(AndroidKeyCode.BACK);
//					//Assert.fail("App Not Found on PlayStore");
//					return false;
//				}
		}catch(Exception e){
			return false;
		}
	}
}
