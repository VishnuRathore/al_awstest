package actionHandler;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class GetElement {
	
	public static AndroidElement ByName(String text, AndroidDriver myApp){
		AndroidElement ae;
		try{
			myApp.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			ae=(AndroidElement) myApp.findElement(By.xpath("//*[@text='"+text+"']"));
			return ae;
		}catch(NoSuchElementException e){
			//e.printStackTrace();
			return null;
		}
	}
	
	public static AndroidElement ByID(AndroidDriver myApp,By by){
		AndroidElement ae;
		myApp.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		try{
			ae=(AndroidElement) myApp.findElement(by);
			return ae;
		}catch(NoSuchElementException e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static AndroidElement Byid(AndroidDriver lockScreen, String id){
		AndroidElement ae;
		lockScreen.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		try{
			ae=(AndroidElement) lockScreen.findElement(By.id(id));
			return ae;
		}catch(NoSuchElementException e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static AndroidElement ByID(AndroidDriver myApp,By by,int delay){
		AndroidElement ae;
		try{
			myApp.manage().timeouts().implicitlyWait(delay,TimeUnit.SECONDS);
			ae=(AndroidElement) myApp.findElement(by);
			return ae;
		}catch(NoSuchElementException e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<AndroidElement> List(AndroidDriver myApp,By by){
		try{
			List<AndroidElement> ae=myApp.findElements(by);
			return ae;
		}catch(NoSuchElementException e){
			e.printStackTrace();
			return null;
		}
	}
}