package actionHandler;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class searchHandler {

	public static void performTextSearch(AndroidDriver myApp,String text) throws Exception{
		AndroidElement ae;
//		  myApp.lockDevice();
//		  Thread.sleep(2000);
//		  myApp.unlockDevice();
		  ae=(AndroidElement) myApp.findElement(By.id("textSearch"));
		  ae.click();
		  ae.sendKeys(text);
		  SwipeHandler.TapRCorner(myApp);
		  Thread.sleep(2000);
		  myApp.longPressKeyCode(AndroidKeyCode.BACK);
	}
}
