package actionHandler;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SwipeHandler {
	
	public static void VerticalSwipeUpScreen(AndroidDriver AppDriver, int Statusbar){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		int DisplayMargin=10;
		AppDriver.swipe(max_X/2,max_Y-Statusbar,max_X/2,0,800);
	}
	
	
	public static void verticalSwipeDownScreen(AndroidDriver AppDriver, int Statusbar){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int DisplayMargin=10;
		AppDriver.swipe(max_X/2,Statusbar,max_X/2,max_Y-DisplayMargin,2000);
	}
	
	public static void VerticalSwipeUpScreenHalf(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int DisplayMargin=10;
		AppDriver.swipe(max_X/2,max_Y/2,max_X/2,DisplayMargin,200);
	}
	
	public static void verticalSwipeDownScreenHalf(AndroidDriver AppDriver, int Statusbar){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int DisplayMargin=10;
		AppDriver.swipe(max_X/2,Statusbar,max_X/2,max_Y/2,2000);
	}
	
	public static void VerticalSwipeUpCell(AndroidDriver AppDriver,int CellHeight){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		AppDriver.swipe(max_X/2,max_Y-CellHeight,max_X/2,max_Y/2,2000);
	}
	
	public static void VerticalSwipeUpElement(AndroidDriver AppDriver,By by){
		AndroidElement AE=(AndroidElement) AppDriver.findElement(by);
		int AE_Y=AE.getCenter().getY();
		int AE_X=AE.getCenter().getX();
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		//AppDriver.swipe(startx, starty, endx, endy, duration);
		AppDriver.swipe(AE_X,AE_Y,AE_X,max_Y/2,100);
	}
	
	public static void verticalSwipeDownElement(AndroidDriver AppDriver, By by){
		AndroidElement AE=(AndroidElement) AppDriver.findElement(by);
		int AE_Y=AE.getCenter().getY();
		int AE_X=AE.getCenter().getX();
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int DisplayMargin=10;
		AppDriver.swipe(AE_X,AE_Y,max_X/2,AE_Y,500);
		//AppDriver.swipe(max_X/2,Statusbar,max_X/2,max_Y-DisplayMargin,2000);
	}
	
	public static void VerticalSwipeUpAndroidElement(AndroidDriver AppDriver,AndroidElement AE){
		int AE_Y=AE.getCenter().getY();
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		AppDriver.swipe(max_X/2,AE_Y+AE_Y/2,max_X/2,AE_Y-AE_Y/2,2000);
	}	
	
	public static void HorizontalSwipeRL(AndroidDriver AppDriver,int ElementCenterX){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int marginX=100;
		AppDriver.swipe(max_X-marginX,max_Y/2,ElementCenterX-ElementCenterX/2,max_Y/2,2000);
	}
	
	public static void HorizontalSwipeLR(AndroidDriver AppDriver,int ElementCenterX){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		AppDriver.swipe(ElementCenterX-ElementCenterX/2,max_Y/2,max_X-100,max_Y/2,2000);
	}
	
	public static void HorizontalSwipeElementRL(AndroidDriver AppDriver,By by){
		AndroidElement AE=(AndroidElement) AppDriver.findElement(by);
		int AE_X=AE.getCenter().getX();
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int marginX=100;
		AppDriver.swipe(max_X-marginX,max_Y/2,AE_X-(AE_X/2+marginX),max_Y/2,2000);
	}
	
	public static void HorizontalSwipeRLScreen(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int marginX=100;
		//AppDriver.swipe(startx, starty, endx, endy, duration);
		AppDriver.swipe(max_X-100,max_Y/2,100,max_Y/2,500);
	}
	
	public static void HorizontalSwipeLRScreen(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int marginX=100;
		//AppDriver.swipe(startx, starty, endx, endy, duration);
		AppDriver.swipe(100,max_Y/2,max_X-100,max_Y/2,1000);
	}
	
	public static void HorizontalSwipeElementLR(AndroidDriver AppDriver,By by){
		AndroidElement AE=(AndroidElement) AppDriver.findElement(by);
		int AE_X=AE.getCenter().getX();
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
		int marginX=100;
		AppDriver.swipe(AE_X-AE_X/2,max_Y/2,max_X-marginX,max_Y/2,2000);
	}
	
	public static void SwipeHLR(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		AppDriver.swipe(10,max_Y/2,max_X-100,max_Y/2,2000);
	}
	
	public static void CenterTap(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		AppDriver.tap(1,max_X/2, max_Y/2, 2000);
	}
	
	public static void TapRCorner(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();	
//		System.out.println("X:"+max_X);
//		System.out.println("Y:"+max_Y);
		if((max_X>=1080)&&(max_Y>=1920))
		{
			AppDriver.tap(1,max_X-100, max_Y-100, 100);
		}
		if(max_X<=780)
		{
			AppDriver.tap(1,max_X-50, max_Y-50, 100);
		}
		//AppDriver.tap(1,max_X-100, max_Y-100, 3000);
	}
	
	public static void TapLCorner(AndroidDriver AppDriver){
		int max_Y=AppDriver.manage().window().getSize().getHeight();
		int max_X=AppDriver.manage().window().getSize().getWidth();		
		AppDriver.tap(1,max_X/2, max_Y/2, 2000);
	}
}
