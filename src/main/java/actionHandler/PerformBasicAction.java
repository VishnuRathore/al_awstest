package actionHandler;

import org.openqa.selenium.By;

import elementHandler.ElementHandler;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class PerformBasicAction {

	public static void Tap(AndroidDriver myApp,By by)
	{
		if(ElementHandler.isElementTappable(myApp, by))
		{
			AndroidElement Ae=ElementHandler.get(myApp,by);
			Ae.click();
		}
		else
		{
			System.out.println("Either Element is not Present or Clickable");
		}
	}
	
	public static void swipeLeftSlidebar(AndroidDriver ad){
		SwipeHandler.SwipeHLR(ad);
	}
	
	public static void swipeUpScreen(AndroidDriver ad){
		SwipeHandler.VerticalSwipeUpScreen(ad,150);
	}
	
	public static void swipeUpElement(AndroidDriver ad, By by){
		SwipeHandler.VerticalSwipeUpElement(ad, by);
	}
	
	public static void waitforTime(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
