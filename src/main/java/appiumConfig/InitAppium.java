/**
 * 
 */
package appiumConfig;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.UnreachableBrowserException;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * @author Vishnu.Rathore
 *  Using this Public Classes you can Initialize the Appium Driver by PackageName and ActivityName
 */
public class InitAppium {
	public enum osType {
	    linux, windows,mac;
	}
	
	public static AndroidDriver launchApp(String packageName,String activityName)
	{
		String localpackageName=packageName;
		String localactivityName=activityName;
		DesiredCapabilities mymob=new DesiredCapabilities();
		mymob.setCapability(MobileCapabilityType.VERSION,DeviceSettings.VERSION);
		mymob.setCapability(MobileCapabilityType.PLATFORM,DeviceSettings.PLATFORM);
		mymob.setCapability("deviceName",DeviceSettings.deviceName);
		mymob.setCapability("app",packageName);
		mymob.setCapability("appActivity",activityName);
//		mymob.setCapability("clearSystemFiles",true);
		
		String detectedOS = System.getProperty("os.name").toLowerCase();
		osType osName = osType.valueOf(detectedOS);
		
		switch(osName){
		
		case linux: 
			//System.out.println("Linux Server");
			try {
				URL url = new URL("http://127.0.0.1:4723/wd/hub");
				AndroidDriver appiumInstance =new AndroidDriver(url, mymob);
				appiumInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				appiumInstance.getContext();
				appiumInstance.context("NATIVE_APP");

				return appiumInstance;
				}
			catch(UnreachableBrowserException ub){
				System.out.println("Error- Appium Client Server is not running..");
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
			
		case mac:
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}
				}
			catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
		
		case windows:
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}}
				catch(Exception ex){
					ex.printStackTrace();
				}
			return null;
		
		default:
				//Assuming the Windows is default Platform
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}
			}catch(NoSuchSessionException nse){
				System.out.println("Appium Session not started!");
				launchApp(packageName,activityName);
			}catch(Exception ex){
					ex.printStackTrace();
				}
			return null;	
		}
	}

	public static AndroidDriver launchCacheApp(String packageName,String activityName)
	{
		String localpackageName=packageName;
		String localactivityName=activityName;
		DesiredCapabilities mymob=new DesiredCapabilities();
		mymob.setCapability(MobileCapabilityType.VERSION,DeviceSettings.VERSION);
		mymob.setCapability(MobileCapabilityType.PLATFORM,DeviceSettings.PLATFORM);
		mymob.setCapability("deviceName",DeviceSettings.deviceName);
		mymob.setCapability("app",packageName);
		mymob.setCapability("appActivity",activityName);
		
		String detectedOS = System.getProperty("os.name").toLowerCase();
		osType osName = osType.valueOf(detectedOS);
		
		switch(osName){
		
		case linux: 
			//System.out.println("Linux Server");
			try {
				mymob.setCapability("noReset",true);
				mymob.setCapability("fullReset",false);
				URL url = new URL("http://127.0.0.1:4723/wd/hub");
				AndroidDriver appiumInstance =new AndroidDriver(url, mymob);
				appiumInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				appiumInstance.getContext();
				appiumInstance.context("NATIVE_APP");

				return appiumInstance;
				}
			catch(UnreachableBrowserException ub){
				System.out.println("Error- Appium Client Server is not running..");
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
			
		case mac:
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}
				}
			catch(Exception ex){
				ex.printStackTrace();
			}
			return null;
		
		case windows:
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}}
				catch(Exception ex){
					ex.printStackTrace();
				}
			return null;
		
		default:
				//Assuming the Windows is default Platform
			try {
				AndroidDriver usscInstance =new AndroidDriver( new URL( "http://127.0.0.1:4723/wd/hub" ), mymob);
				usscInstance.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				if(usscInstance!=null)
				{
					return usscInstance;
				}
				
				else
				{
					launchApp(localpackageName,localactivityName);
				}}
				catch(Exception ex){
					ex.printStackTrace();
				}
			return null;	
		}
	}

}
