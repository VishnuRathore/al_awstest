package lockscreen.test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import actionHandler.GetElement;
import actionHandler.PerformAdvanceAction;
import actionHandler.SwipeHandler;
import appScreens.ALSettingsScreen;
import appScreens.HomeScreen;
import appScreens.LockscreenMain;
import appScreens.navigation;
import appiumConfig.InitAppium;
import elementHandler.ElementHandler;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class LockScreenUITest extends HomeScreen{
	static AndroidDriver lockScreen;
	AndroidElement ae;
	ALSettingsScreen setting;
	LockscreenMain ls;
	String searchKeyword="hotels";
	
  //@BeforeTest
  public void Activation() throws NoSuchElementException{
	  lockScreen=InitAppium.launchApp("com.airfind.lockscreen","activity.LauncherActivity");
	  GetElement.ByID(lockScreen,By.id("get_started_btn_on_first_launch")).click();
	  GetElement.ByID(lockScreen,By.id("switchWidget")).click();
	  GetElement.ByID(lockScreen,By.id("button1")).click();
	  GetElement.ByID(lockScreen,By.id("summary")).click();
	  lockScreen.closeApp();
////	  GetElement.ByName("GET STARTED", lockScreen).click();
//	  GetElement.ByName("Launch Screen", lockScreen).click();
//	  try{
//		  if(ElementHandler.isElementPresentByName("OK",lockScreen)){
//			  GetElement.ByName("OK", lockScreen).click();}
//		  else{
//			  GetElement.ByName("ALLOW", lockScreen).click();}
//	  }catch(Exception e){
//		  //Hit Button by ID
//	  }
//	  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
//	  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
//	  if(lockScreen.currentActivity().equalsIgnoreCase(".ChooseLockGeneric"))
//	  {
//		  //Disable default lockscreen
//		  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
//	  }
//	  try{
//	  WebElement myDynamicElement = (new WebDriverWait(lockScreen,60))
//			  .until(ExpectedConditions.presenceOfElementLocated(By.id("sliding_layout")));//sliding_layout
//	  PerformAdvanceAction.takeScreenshot(lockScreen,"Lockscreen_"+lockScreen.getDeviceTime());
//	  }catch(Exception e){
//		  
//		  System.out.println("Failed to Launch App - Refreshing Launch screen");
//		  lockScreen.pressKeyCode(AndroidKeyCode.KEYCODE_POWER);
//		  lockScreen.pressKeyCode(AndroidKeyCode.KEYCODE_POWER); 
//		  PerformAdvanceAction.takeScreenshot(lockScreen,"#ErrorLaunch_"+lockScreen.getDeviceTime());
//	  }
	  
  }
  
  // Feature Ads Tiles  
  @Test(priority=1)
  public void FeatureAdsTilesTest() throws Exception{
	  List<AndroidElement> tiles=lockScreen.findElements(By.id("tileImg"));
	  for(AndroidElement t:tiles){
		  t.click();
		  Thread.sleep(2000);
		  System.out.println(lockScreen.currentActivity());
		  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		  GetElement.ByID(lockScreen,By.id("exitButton")).click();}
  }
  
  // Test News Screen
//  @Test(priority=2)
  public void NewsUITest(){
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
  }
  
  //Test Weather Screen
  @Test(priority=3)
  public void Weather_cityNameTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("cityName"));
	  Assert.assertNotNull(ae,"Element visibility ");
	 String value=ae.getText();
	 if(value.compareToIgnoreCase("--")==0 || value==null){
		 Assert.fail("Invalid Data");
		 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	 }
  }
  
  @Test(priority=4)
  public void Weather_cityTempTest()throws NoSuchElementException {
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("curTemp"));
	 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=5)
  public void Weather_TempIconTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("curTempIcn_1"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=6)
  public void Weather_TempLabelTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("curStatus"));
	 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=7)
  public void Weather_TempProviderTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("weatherProvider"));
		 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=8)
  public void Weather_WindDetailTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("windText"));
		 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=9)
  public void Weather_HumidityDetailsTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("humidityText"));
		 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=10)
  public void Weather_RainPredictionTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("rainText"));
	 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=11)
  public void Weather_MorningTempTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("morningTempText"));
	 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");
			   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=12)
  public void Weather_EveningTempTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("eveningTempText"));
	 String value=ae.getText();
		 if(value.compareToIgnoreCase("--")==0 || value==null){
			 Assert.fail("Invalid Data");	  
			 PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		 }
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=13)
  public void Weather_BigIconTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  ae=GetElement.ByID(lockScreen,By.id("curTempIcn_2"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  Assert.assertNotNull(ae,"Element visibility ");
  }
  
  @Test(priority=14)
  public void Weather_HourlyTest()throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  SwipeHandler.VerticalSwipeUpElement(lockScreen,By.id("hourlyRowContainer"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		  List<AndroidElement> hrsLbl=lockScreen.findElements(By.id("hourName"));
		  
		  //Test Hours Labels
		  Assert.assertEquals(hrsLbl.size(),5);
		  
		  	for(AndroidElement ae:hrsLbl){
		  		Assert.assertNotNull(ae.getText(),"Hours Visibililty");
		  		 String value=ae.getText();
				 if(value.compareToIgnoreCase("--")==0 || value.contains("..")){
					 Assert.fail("Invalid Data");
					 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
				 }
		  	}
		  	
		  //Test Weather Hourly Icon	
		  List<AndroidElement> hrsIcn=lockScreen.findElements(By.id("hourIcon"));
		  	for(AndroidElement ae:hrsIcn){
		  		Assert.assertNotNull(ae,"Icons Visibililty");
		  	}
		  	
		  //Test Weather Hourly Temp 	
		  List<AndroidElement> hrsTmp=lockScreen.findElements(By.id("temp"));
		  	for(AndroidElement ae:hrsTmp){
		  		Assert.assertNotNull(ae.getText(),"Temp Visibililty");
		  		 String value=ae.getText();
				 if(value.compareToIgnoreCase("--")==0 || value.contains("..")){
					 Assert.fail("Invalid Data");
					 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
				 }
		  	}
  }
  
  @Test(priority=15)
  public void Weather_HoulyRedirectionTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  SwipeHandler.VerticalSwipeUpElement(lockScreen,By.id("hourlyRowContainer"));
	  	ae=GetElement.ByID(lockScreen,By.id("moreHourly"));
	  	if(ae==null){
	  		Assert.fail("Element not visible");
	  		PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  	}
	  	else{
	  	ae.click();
	  	Assert.assertNotNull(GetElement.ByID(lockScreen,By.id("addressBar"),10));
	  	PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  	GetElement.ByID(lockScreen,By.id("exitButton")).click();
	  	}
  }
  
  @Test(priority=16)
  public void Weather_WeekForecastTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  SwipeHandler.VerticalSwipeUpElement(lockScreen,By.id("hourlyRowContainer"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  List<AndroidElement> weekName=lockScreen.findElements(By.id("weekName"));
		  
		  //Test Week Name Labels
		  	for(AndroidElement ae:weekName){
		  		Assert.assertNotNull(ae.getText(),"Week Name Visibililty");
		  		
		  		 String value=ae.getText();
				 if(value.compareToIgnoreCase("--")==0 || value.contains("..")){
					 Assert.fail("Invalid Data");
					 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
				 }
		  	}
		  	Assert.assertEquals(weekName.size(),6,"Week Days Name");
		  	
		  //Test Weather Week Weather Icon	
		  List<AndroidElement> weekIcon=lockScreen.findElements(By.id("weekIcon"));
		  	for(AndroidElement ae:weekIcon){
		  		Assert.assertNotNull(ae,"Icons Visibililty");
		  	}
		  	
		  //Test Weather Week Max Temp 	
		  List<AndroidElement> maxTemp=lockScreen.findElements(By.id("maxTemp"));
		  	for(AndroidElement ae:maxTemp){
		  		Assert.assertNotNull(ae.getText(),"Week Max Temp Visibililty");
		  		 String value=ae.getText();
				 if(value.compareToIgnoreCase("--")==0 || value.contains("..")){
					 Assert.fail("Invalid Data");
					 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
				 }
		  	}
		  	
		  //Test Weather Week MIn Temp 	
		  List<AndroidElement> minTemp=lockScreen.findElements(By.id("minTemp"));
		  	for(AndroidElement ae:minTemp){
		  		Assert.assertNotNull(ae.getText(),"Week Min Temp Visibililty");
		  		 String value=ae.getText();
				 if(value.compareToIgnoreCase("--")==0 || value.contains("..")){
					 Assert.fail("Invalid Data");
					 	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
				 }
		  	}
  }
  
  @Test(priority=17)
  public void Weather_WeeklyRedirectionTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  SwipeHandler.VerticalSwipeUpElement(lockScreen,By.id("hourlyRowContainer"));
	  	ae=GetElement.ByID(lockScreen,By.id("moreWeek"));
	  	if(ae==null){
	  		Assert.fail("Element not visible");
	  		PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  	}
	  	else{
	  	ae.click();
	  	Assert.assertNotNull(GetElement.ByID(lockScreen,By.id("addressBar"),10));
	  	PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  	GetElement.ByID(lockScreen,By.id("exitButton")).click();
	  	}
  }
  
  @Test(priority=18)
  	public void Weather_RefreshTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='3']")).click();
	  GetElement.ByID(lockScreen,By.id("refresh")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  
  }
  
 // SEARCH FEATURE TEST
  @Test (priority=19)
	public void TextSearchTest() throws Exception{
		  ae=GetElement.ByID(lockScreen,By.id("textSearch"));
		  ae.tap(1,500);
		  ae.sendKeys(searchKeyword);
		  ae=GetElement.ByID(lockScreen,By.id("cameraBtn"));
		  lockScreen.tap(2,ae,800);
		  	String serpURL=GetElement.ByID(lockScreen,By.id("addressBar"),10).getText();
		  	Assert.assertTrue(serpURL.contains(searchKeyword));
		   PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
			  
	}
  
  @Test (priority=20)
	public void SearchAutocompleteTest() throws Exception{
	  String searchKeyword="hotels";
	  ae=GetElement.ByID(lockScreen,By.id("textSearch"));
	  ae.tap(1,500);
	  ae.sendKeys(searchKeyword);
	  ae.clear(); //Search Auto complete Bug
	  ae.sendKeys(searchKeyword);
	  Thread.sleep(500);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	}
  
  @Test(priority=21)
  	public void VoiceSearchTest() throws Exception{
	  GetElement.ByID(lockScreen,By.id("micIcon")).click();
	  Assert.assertNotNull(GetElement.ByID(lockScreen,By.id("outSideClickVoiceSearch")));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());

  }
  
  @Test(priority=22)
  	public void News_NavigationTest() throws Exception{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  for(int next=0;next<10;next++) { 
		  Thread.sleep(500);
		  	for(int i=0;i<15;i++){	
		  		SwipeHandler.VerticalSwipeUpScreen(lockScreen,400);}
		  		Thread.sleep(500);
		  		PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
		  		SwipeHandler.HorizontalSwipeRLScreen(lockScreen); }
	  
//	  for(int prev=0;prev<10;prev++){
//		  Thread.sleep(1000);
//		  	for(int i=0;i<15;i++){	
//		  		SwipeHandler.verticalSwipeDownScreen(lockScreen,400);}
//		  Thread.sleep(500);
//		  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
//		  SwipeHandler.HorizontalSwipeLRScreen(lockScreen);}
  }
  	
  @Test(priority=23)
  	public void News_ImageTest(){
	 try{ 
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  for(int next=0;next<10;next++) { 
		  List<AndroidElement> newsIcon=lockScreen.findElements(By.id("newsImage"));
		  for(AndroidElement ae:newsIcon){
			  Assert.assertNotNull(ae);}
		  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);}
		}catch(Exception e){
			PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());}
  }
  
  @Test(priority=24)
  public void News_TitleTest(){
	 try{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
		  	for(int next=0;next<10;next++) { 
			  List<AndroidElement> newsTitle=lockScreen.findElements(By.id("newsTitle"));
			  for(AndroidElement ae:newsTitle){
				  Assert.assertNotNull(ae);}
		   SwipeHandler.HorizontalSwipeRLScreen(lockScreen);}
   		}catch(Exception e){
		PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());}
  }
  
  @Test(priority=25)
  public void News_ProviderTest(){
	try{  
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  for(int next=0;next<10;next++) { 
		  List<AndroidElement> newsProvider=lockScreen.findElements(By.id("newsProvider"));
		  for(AndroidElement ae:newsProvider){
			  Assert.assertNotNull(ae);}
		  SwipeHandler.HorizontalSwipeRLScreen(lockScreen); }
		}catch(Exception e){
	PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());}

  }
  
  @Test(priority=26)
  public void News_SponsoredTest(){
	try{  
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  for(int next=0;next<10;next++) { 
		if (GetElement.ByID(lockScreen,By.id("sponsered"))==null){
			GetElement.ByID(lockScreen,By.id("refresh")).click();}  
		  List<AndroidElement> sponsered=lockScreen.findElements(By.id("sponsered"));
		  for(AndroidElement ae:sponsered){
			  Assert.assertNotNull(ae);}
		  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);}
		}catch(Exception e){
	PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());}
  }
  
  @Test(priority=27)
  public void News_RedirectionTest() throws NoSuchElementException{
	 try{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
		  	for(int next=0;next<10;next++) { 
			  List<AndroidElement> newsTitle=lockScreen.findElements(By.id("newsTitle"));
			  for(AndroidElement ae:newsTitle){
				  ae.click();
				  lockScreen.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
				  //Internal Browser Redirection Handler
					PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
					GetElement.ByID(lockScreen,By.id("exitButton")).click();
				}
		   SwipeHandler.HorizontalSwipeRLScreen(lockScreen);}
   		}catch(Exception e){
		PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());}
  }
  
  @Test(priority=28)
  public void News_CategoriesTest() throws NoSuchElementException{
	  
  }
  
  @Test(priority=29)
  public void News_RefreshTest() throws NoSuchElementException{
	  GetElement.ByID(lockScreen,By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout[@index='4']")).click();
	  GetElement.ByID(lockScreen,By.id("refresh")).click();
  }
  
  @Test(priority=30)
  public void News_MainBrowserAccessTest() throws NoSuchElementException{
	  
  }
  @Test(priority=31)
  public void SwipeRightTest() throws Exception{
	  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());

  }
  
  @Test(priority=32)
  	public void SwipeRight_CameraTest() throws NoSuchElementException{
	  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);
	  GetElement.ByID(lockScreen,By.id("cameraBtn")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
  }
  
  @Test(priority=33)
  	public void SwipeRight_SettingTest() throws Exception{
	  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());	  
  }
  
  @Test(priority=34)
  	public void SwipeRight_UnlockTest() throws Exception{
	  SwipeHandler.HorizontalSwipeRLScreen(lockScreen);
	  SwipeHandler.VerticalSwipeUpElement(lockScreen,By.id("swipeUnlock"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());	  

  }
	
  // DropDown Menu Test Cases  
 @Test (priority=35)
 	public void PullDownMenu_Open() throws Exception {
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
  }
  
  @Test(priority=36)
  public void PDM_WiFi_Enable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_wifi")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=37)
  public void PDM__WiFi_Disable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_wifi")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=38)
  public void PDM_Bluetooth_Enable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_blutooth")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=39)
  public void PDM_Bluetooth_Disable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_blutooth")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=40)
  public void PDM_Airplane_Enable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_airplan")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=41)
  public void PDM_Airplane_Disable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_airplan")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=42)
  public void PDM_Mute() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_mute")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=43)
  public void PDM_Unmute() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_mute")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=44)
  public void PDM_Brightnesss_Increase() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_brightness")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=45)
  public void PDM_Brightnesss_Descrese() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_brightness")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=46)
  public void PDM_Location_Enable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_location")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=47)
  public void PDM_Location_Disable() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_location")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=48)
  public void PDM_Camera() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_selfeeCamera")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  @Test(priority=49)
  public void PDM_Settings() throws Exception{
	  SwipeHandler.verticalSwipeDownElement(lockScreen,By.id("menuTrigger"));
	  GetElement.ByID(lockScreen,By.id("btn_setting")).click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
 
  }
  
  
  @Test(priority=50)
  public void OpenSettings() throws Exception{
	  ae=GetElement.ByID(lockScreen,By.id("setttingBtn"));
	  ae.click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,"OpenSettings_"+lockScreen.getDeviceTime());
	
  }
  
  @Test(priority=51)
  public void Settings_DisableLockscreen() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click();
	  ae=GetElement.ByID(lockScreen,By.id("mainSwitcher"));
	  if(ae.getAttribute("checked").equalsIgnoreCase("false")){
		ae.click();}
	  
	 // Disable Lockscreen 
	  ae.click();
	  Thread.sleep(500);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  //Verify Lockscreen is not enabled
	  sleepDevice(1);
	  Assert.assertNotEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
  }
  
  @Test(priority=52)
  	public void Settings_EnableLockScreen() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click();
	  ae=GetElement.ByID(lockScreen,By.id("mainSwitcher"));
	  if(ae.getAttribute("checked").equalsIgnoreCase("true")){
		ae.click();}
	  
	 // Disable Lockscreen 
	  ae.click();
	  Thread.sleep(500);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	//Verify Lockscreen is not enabled
	  sleepDevice(1);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.SettingsActivity");
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
  }
  
  @Test(priority=52)
  public void Settings_EnableSecurityPin(){
	  GetElement.Byid(lockScreen,"setttingBtn").click();
	  GetElement.Byid(lockScreen,"settings_security").click();
	  GetElement.Byid(lockScreen,"security_pin_linear_layout").click();
	  
	  //Enter PIN Code
	  enterPin(lockScreen);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  GetElement.Byid(lockScreen,"ok_button_on_pin").click();
	  //Verify PIN Code
	  enterPin(lockScreen);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  GetElement.Byid(lockScreen,"ok_button_on_pin").click();
	  //Cancel PIN Recovery Email
	  GetElement.Byid(lockScreen,"button2").click();
	  Assert.assertEquals(GetElement.Byid(lockScreen,"security_description_ftv").getText(),"PIN");
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());

  }
  
  @Test(priority=53)
  public void Settings_DisableSecurityPin(){
	  GetElement.Byid(lockScreen,"setttingBtn").click();
	  enterPin(lockScreen);
	  GetElement.Byid(lockScreen,"settings_security").click();
	  enterPin(lockScreen);
	  GetElement.Byid(lockScreen,"security_none_linear_layout").click();
	  Assert.assertEquals(GetElement.Byid(lockScreen,"security_description_ftv").getText(),"None");
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  //backgroundLockScreen
  }
  
  @Test(priority=54)
  public void Settings_UnlockPIN(){
	  
  }
  
  @Test(priority=55)
  public void Settings_LockDelay_None() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click();  
	  setting.settings_lock_delay.click();
	  setting.lock_delay_none.click();
	  sleepDevice(1);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=56)
  public void Settings_LockDelay_30Sec() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_30_seconds.click();
	  Assert.assertEquals("30 Seconds", setting.lock_delay_time_ftv.getText());
	  sleepDevice(30);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=57)
  public void Settings_LockDelay_1Min() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_1_minute.click();
	  Assert.assertEquals("1 Minute", setting.lock_delay_time_ftv.getText());
	  sleepDevice(60);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=58)
  public void Settings_LockDelay_2Min(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_2_minute.click();
	  Assert.assertEquals("2 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(120);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=59)
  public void Settings_LockDelay_3Min(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_3_minutes.click();
	  Assert.assertEquals("3 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(180);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=60)
  public void Settings_LockDelay_4Min(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_4_minutes.click();
	  Assert.assertEquals("4 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(240);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=61)
  public void Settings_LockDelay_5Min(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  setting.settings_lock_delay.click();
	  setting.lock_delay_5_minutes.click();
	  Assert.assertEquals("5 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(300);
	  Assert.assertEquals(lockScreen.currentActivity(),".activity.LockScreenActivity");
  }
  
  @Test(priority=62)
  public void Settings_DataSavingEnableTest()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.ByID(lockScreen,By.id("settings_data_saving_mode")).click();
	  ae=GetElement.Byid(lockScreen,"switcher");
	  if(ae.getAttribute("checked").equalsIgnoreCase("true")){
		  ae.click();
	  }
	  ae.click();
	  sleepDevice(1);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  Assert.assertTrue(ElementHandler.isElementPresent(lockScreen, By.id("dataSavingFeature")),"Data Saving Icon not visible");
  }
  
  @Test(priority=63)	
  public void Settings_DataSavingModeDisableTest()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.ByID(lockScreen,By.id("settings_data_saving_mode")).click();
	  ae=GetElement.Byid(lockScreen,"switcher");
	  if(ae.getAttribute("checked").equalsIgnoreCase("false")){
		  ae.click();
	  }
	  ae.click();
	  sleepDevice(1);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  Assert.assertFalse(ElementHandler.isElementPresent(lockScreen, By.id("dataSavingFeature")),"Data Saving Icon not visible");
	  
}
 
  @Test(priority=64)
  public void Settings_NotificationsEnable()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.ByID(lockScreen,By.id("settings_notification")).click();
	  ae=GetElement.Byid(lockScreen,"notification_on_off_indicator");
	  if(ae.getAttribute("checked").equalsIgnoreCase("false")){
		  ae.click();
	  }
	  ae.click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());

  }
  
  @Test(priority=65)
  public void Settings_NotificationsVisibiltyHide()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.ByID(lockScreen,By.id("settings_notification")).click();
	  ae=GetElement.Byid(lockScreen,"notification_on_off_indicator");
	  if(ae.getAttribute("checked").equalsIgnoreCase("false")){
		  ae.click();
	  }
	  GetElement.Byid(lockScreen,"notification_visibility_button_linear_layout").click();
	  GetElement.Byid(lockScreen,"hide_sensitive_notification_selected_indicator").click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  GetElement.Byid(lockScreen,"save_notification_visibility_setting_btn").click();
  }
  
  @Test(priority=66)
  public void Settings_NotificationsVisibiltyShow()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.ByID(lockScreen,By.id("settings_notification")).click();
	  ae=GetElement.Byid(lockScreen,"notification_on_off_indicator");
	  if(ae.getAttribute("checked").equalsIgnoreCase("false")){
		  ae.click();
	  }
	  GetElement.Byid(lockScreen,"notification_visibility_button_linear_layout").click();
	  GetElement.Byid(lockScreen,"show_all_notification_selected_indicator").click();
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());
	  GetElement.Byid(lockScreen,"save_notification_visibility_setting_btn").click();
  }
  
  @Test(priority=67)
  public void Settings_WallpaperCustom()throws Exception{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.Byid(lockScreen,"settings_wallpaper").click();
	  GetElement.Byid(lockScreen,"custom_wallpaper_selection_indicator").click();
	  lockScreen.tap(1,800,800,500);
	  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
	  PerformAdvanceAction.takeScreenshot(lockScreen,new Object(){}.getClass().getEnclosingMethod().getName()+lockScreen.getDeviceTime());

  }
  
  
  @Test(priority=68)
  public void Settings_TCRedirection(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  SwipeHandler.VerticalSwipeUpScreen(lockScreen,200);
	  GetElement.Byid(lockScreen,"settings_terms_and_condition").click();
	  String link=GetElement.Byid(lockScreen,"setting_screen_toolbar_title_text").getText();
	  Assert.assertNotNull(link);
  }
 
  @Test(priority=69)
  public void Settings_PrivacyRedirection(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  SwipeHandler.VerticalSwipeUpScreen(lockScreen,200);
	  GetElement.Byid(lockScreen,"settings_privacy").click();
	  String link=GetElement.Byid(lockScreen,"setting_screen_toolbar_title_text").getText();
	  Assert.assertNotNull(link);
 }
 
  @Test(priority=70)
  public void Settings_VersionName(){
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  SwipeHandler.VerticalSwipeUpScreen(lockScreen,200);
	  Assert.assertNotNull(GetElement.Byid(lockScreen,"settings_version").getText());
 }
  
  @Test(priority=71)
  public void Home_WifiIcon()throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.wifiStrength);
  }
  
  @Test(priority=72)
  public void Home_MobileIcon() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.signalStrength);
  }
  
  @Test(priority=73)
  public void Home_BatteryIcon() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.signalStrength);
  }
  
  @Test(priority=74)
  public void Home_BatteryPercentage() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.batteryPercantage);
  }
  
  @Test(priority=75)
  public void Home_Clock() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.hour_FTV);
  }
  
  @Test(priority=76)
  public void Home_Date() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.day_and_date_FTV);
  }
  
  @Test(priority=77)
  public void Home_AdsTiles() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.tilesContainer);
  }
  
  @Test(priority=78)
  public void Home_SwitchScreen() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  hs.radio1.click();
	  Assert.assertNotNull(hs.radio1);
  }
  
  @Test(priority=79)
  public void Home_Pagination() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  hs.radio2.click();
	  Assert.assertNotNull(hs.radio2);
  }
  
  @Test(priority=80)
  public void Home_SearchBar() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  hs.textSearch.click();
	  Assert.assertNotNull(hs.textSearch);
  }
  
  @Test(priority=81)
  public void Home_VoiceSearch() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.micIcon);
  }
  
  @Test(priority=82)
  public void Home_HintText() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.swipeUnlock);
  }
  
  @Test(priority=83)
  public void Home_SettingsIcon() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.setttingBtn);
  }
  
  @Test(priority=84)
  public void Home_LockIcon() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.unlockBtn);
  }
  
  @Test(priority=85)
  public void Home_CameraIcon() throws Exception{
	  HomeScreen hs=PageFactory.initElements(lockScreen,HomeScreen.class);
	  Assert.assertNotNull(hs.cameraBtn);
  }
  
  
  @BeforeMethod
  public void beforeMethod() {
	  lockScreen=InitAppium.launchApp("com.airfind.lockscreen","activity.LauncherActivity");
	  if(GetElement.ByID(lockScreen,By.id("get_started_btn_on_first_launch"))!=null){
		  GetElement.ByID(lockScreen,By.id("get_started_btn_on_first_launch")).click();
		  GetElement.ByID(lockScreen,By.id("switchWidget")).click();
		  GetElement.ByID(lockScreen,By.id("button1")).click();
		  GetElement.ByID(lockScreen,By.id("summary")).click();
	  }
//	  System.out.println("Session Started at: "+lockScreen.getDeviceTime()+"Session ID: "+lockScreen.getSessionId()+" Activity Name: "+lockScreen.currentActivity());
//	  setting = PageFactory.initElements(lockScreen, ALSettingsScreen.class);
//	  ls = PageFactory.initElements(lockScreen, LockscreenMain.class);
	  }

  @AfterMethod
  public void afterMethod() {
	  //lockScreen.removeApp("com.airfind.lockscreen");
//	  lockScreen.close();
  }

  //Test Helper
  public void sleepDevice(int seconds){
	  //Lock Device
	  try {
		  lockScreen.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		  Thread.sleep(seconds*1000);
	  	 } catch (InterruptedException e) {
		// TODO Auto-generated catch block
	  		  lockScreen.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		e.printStackTrace();
	}
	  lockScreen.unlockDevice();
  }
   
	 public void enterPin(AndroidDriver lockScreen){
		  GetElement.Byid(lockScreen,"button1").tap(1,100);
		  GetElement.Byid(lockScreen,"button2").tap(1,100);
		  GetElement.Byid(lockScreen,"button3").tap(1,100);
		  GetElement.Byid(lockScreen,"button4").tap(1,100);
	 }	 
  }
