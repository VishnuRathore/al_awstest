package lockscreen.test;

import java.util.List;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import actionHandler.GetElement;
import actionHandler.PerformAdvanceAction;
import actionHandler.SwipeHandler;
import appScreens.ALSettingsScreen;
import appScreens.LockscreenMain;
import appiumConfig.InitAppium;
import elementHandler.ElementHandler;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class TestRunner {
	 static AndroidDriver lockScreen;
	AndroidElement ae;
	ALSettingsScreen setting;
	LockscreenMain ls;
	
  @Test
  public void f() throws InterruptedException{
	  GetElement.ByID(lockScreen,By.id("setttingBtn")).click(); 
	  GetElement.Byid(lockScreen,"settings_wallpaper").click();
	  GetElement.Byid(lockScreen,"custom_wallpaper_selection_indicator").click();
	  lockScreen.tap(1,800,800,500);
	  
	  lockScreen.pressKeyCode(AndroidKeyCode.BACK);
  }
 

//  
  @AfterMethod
  public void afterMethod() {
		lockScreen.closeApp();
  }

  @BeforeMethod
  public void BeforeMethod() {
	  lockScreen=InitAppium.launchApp("com.airfind.lockscreen","activity.LauncherActivity");
  }
  
  public void sleepDevice(int seconds){
	  //Lock Device
	  try {
		  lockScreen.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		  Thread.sleep(seconds*1000);
	  	 } catch (InterruptedException e) {
		// TODO Auto-generated catch block
	  		  lockScreen.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		e.printStackTrace();
	}
	  lockScreen.unlockDevice();
  }

}
