package lockscreen.test;

import org.testng.annotations.Test;

import actionHandler.SwipeHandler;
import actionHandler.searchHandler;
import appiumConfig.InitAppium;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;

public class DemoTest {
	AndroidDriver myApp;
	AndroidElement ae;
  @Test(priority=1)
  public void LSTextSearch() throws Exception{
	  for(int i=0;i<=10;i++){
	  searchHandler.performTextSearch(myApp, "Find me the nearest hotel with pickup and drop off");
	  }
  }
  
 
  @Test(priority=2)
  public void LSVoiceSearch() throws Exception{
	  ae=(AndroidElement) myApp.findElement(By.id("micIcon"));
	  ae.click();
  }
  
  @Test(priority=3)
  public void LSFacebookTest() throws Exception{
	  ae=(AndroidElement) myApp.findElementByXPath("//android.support.v7.widget.RecyclerView/android.widget.ImageView[@index='0']");
	  ae.click();
  }
  
  @Test(priority=4)
  public void LSTwitterTest() throws Exception{
	  ae=(AndroidElement) myApp.findElementByXPath("//android.support.v7.widget.RecyclerView/android.widget.ImageView[@index='1']");
	  ae.click();
  }
  
  @Test(priority=5)
  public void LSWeatherTest() throws Exception{
	  ae=(AndroidElement) myApp.findElementByXPath("//android.support.v7.widget.RecyclerView/android.widget.ImageView[@index='3']");
	  ae.click();
  }
  
  @Test(priority=6)
  public void LSNewsTest() throws Exception{
	  ae=(AndroidElement) myApp.findElementByXPath("//android.support.v7.widget.RecyclerView/android.widget.ImageView[@index='4']");
	  ae.click();
  }
  
  @Test(priority=7)
  public void LSNewsFeedTest() throws Exception{
	  Thread.sleep(2000);
	  SwipeHandler.HorizontalSwipeRLScreen(myApp);
	  Thread.sleep(2000);
	  SwipeHandler.HorizontalSwipeLRScreen(myApp);
  }
  
  @Test(priority=8)
  public void LSTapUnlockTest() throws Exception{
	  ae=(AndroidElement) myApp.findElement(By.id("unlockBtn"));
	  ae.click();
  }
	
  @Test(priority=9)
  public void LSSettingsTest() throws Exception{
	  ae=(AndroidElement) myApp.findElement(By.id("setttingBtn"));
	  ae.click();
  }
  
  @Test(priority=10)
  public void LSCameraTest() throws Exception{
	  ae=(AndroidElement) myApp.findElement(By.id("cameraBtn"));
	  ae.click();
  }
  
  @Test(priority=11)
  public void LSTimeTest() throws Exception{
	  String time=myApp.findElement(By.id("hour_FTV")).getText();
	  String median=myApp.findElement(By.id("ampm_FTV")).getText();
	  String dayDate=myApp.findElement(By.id("day_and_date_FTV")).getText();
	  System.out.println(time);
	  System.out.println(median);
	  System.out.println(dayDate);
	  System.out.println(myApp.getDeviceTime());
  }
  
  @Test(priority=12)
  public void LSwipeUnlockTest() throws Exception{
	  
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  myApp=InitAppium.launchCacheApp("com.airfind.lockscreen",".activity.LockScreenActivity");
  }

  @AfterMethod
  public void afterMethod() {
  }

}
