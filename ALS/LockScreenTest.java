package lockscreen.test;

import org.testng.annotations.Test;
import actionHandler.SwipeHandler;
import appScreens.ALSettingsScreen;
import appiumConfig.InitAppium;
import elementHandler.GetElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;

public class LockScreenTest {
	AndroidDriver myApp;
	WebElement ae;
	ALSettingsScreen setting;
	
	//com.airfind.lockscreen/.activity.FirstRunActivity - Tutorial
	
  @Test(priority=1)
  public void TestSecuritySettings_None() throws Exception {
	  setting.settings_security.click();
	  
	  setting.security_none_linear_layout.click();
	  
	  Assert.assertEquals("None", setting.security_description_ftv.getText());
	  sleepDevice(1);
	  System.out.println("Device Unlocked");
	  ALTutorial();
	  System.out.println("Tutorial Skipped");
	  swipeunlockDevice();
  }
  
  @Test(priority=2)
  public void TestSecuritySettings_PIN(){
	  if(setting.security_description_ftv.getText().equalsIgnoreCase("None"))
	  {  
		  setting.settings_security.click();
		  setting.security_pin_linear_layout.click();

		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  setting.ok_button_on_pin.click();
	  
		  //VERIFY PIN Lock Code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  myApp.findElement(By.id("ok_button_on_pin")).click();
	  
	  //Set Passcode Recovery E-mail
	  
	  AndroidElement recoveryEmail=(AndroidElement) myApp.findElement(By.id("passcode_recovery_email_et"));
	  recoveryEmail.clear();
	  recoveryEmail.sendKeys("vishnu.rathore@ideavate.com");
	  GetElement.ByName("NO, THANKS", myApp).click(); //SEND
	  
	  //Email PIN Code
	  
	//	  GetElement.ByName("Gmail", myApp).click();
	//	  Thread.sleep(2000);
	//	  AndroidElement GmailSend=(AndroidElement) myApp.findElement(By.xpath("//android.support.v7.widget.bh/android.widget.TextView[@index='1']"));
	//	  GmailSend.click();
	  
	  Assert.assertEquals("Pin", setting.security_description_ftv.getText());
	  sleepDevice(1);
	  ALTutorial();
	  swipePinUnlockDevice();
	  }
	  else{
		  throw new SkipException("PIN Security is already active!");
		  }
  }
  
  @Test(priority=3)
  public void TestSecuritySettings_RESET_PIN(){
	  if(setting.security_description_ftv.getText().equalsIgnoreCase("None"))
	  {
		  setting.settings_security.click();
		  setting.security_pin_linear_layout.click();

		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  setting.ok_button_on_pin.click();
	  
		  //VERIFY PIN Lock Code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  myApp.findElement(By.id("ok_button_on_pin")).click();
	  
	  //Set Passcode Recovery E-mail
	  
		  AndroidElement recoveryEmail=(AndroidElement) myApp.findElement(By.id("passcode_recovery_email_et"));
		  recoveryEmail.clear();
	  	  recoveryEmail.sendKeys("vishnu.rathore@ideavate.com");
	  	  GetElement.ByName("NO, THANKS", myApp).click(); //SEND  
	  }
		  setting.settings_security.click();
		  
		  setting.security_pin_linear_layout.click();
		  
		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
//		  setting.ok_button_on_pin.click();
		  
		  setting.security_pin_linear_layout.click();

		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  setting.ok_button_on_pin.click();
	  
		  //VERIFY PIN Lock Code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  myApp.findElement(By.id("ok_button_on_pin")).click();
	  
		  //Set Passcode Recovery E-mail
	  
		  AndroidElement recoveryEmail=(AndroidElement) myApp.findElement(By.id("passcode_recovery_email_et"));
		  recoveryEmail.clear();
		  recoveryEmail.sendKeys("vishnu.rathore@ideavate.com");
		  GetElement.ByName("NO, THANKS", myApp).click(); //SEND
			  
  }
  
  @Test(priority=4)
  public void TestSecuritySettings_REMOVE_PIN()
  {
	  if(setting.security_description_ftv.getText().equalsIgnoreCase("None"))
	  {
		  
		  setting.settings_security.click();
		  setting.security_pin_linear_layout.click();

		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  setting.ok_button_on_pin.click();
	  
		  //VERIFY PIN Lock Code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
		  myApp.findElement(By.id("ok_button_on_pin")).click();
	  
	  //Set Passcode Recovery E-mail
	  
		  AndroidElement recoveryEmail=(AndroidElement) myApp.findElement(By.id("passcode_recovery_email_et"));
		  recoveryEmail.clear();
	  	  recoveryEmail.sendKeys("vishnu.rathore@ideavate.com");
	  	  GetElement.ByName("NO, THANKS", myApp).click(); //SEND  
	  }
		  setting.settings_security.click();
		  
		  setting.security_pin_linear_layout.click();
		  
		  // SET PIN Lock code
		  GetElement.ByName("1", myApp).click();
		  GetElement.ByName("2", myApp).click();
		  GetElement.ByName("3", myApp).click();
		  GetElement.ByName("4", myApp).click();
//		  setting.ok_button_on_pin.click();
		  
		  setting.security_none_linear_layout.click();
		  
		  Assert.assertEquals("None", setting.security_description_ftv.getText());
  }
  
  @Test(priority=5)
  public void TestLockDelay_None() throws InterruptedException{
	  setting.settings_lock_delay.click();
	  setting.lock_delay_none.click();
	  Assert.assertEquals("None", setting.lock_delay_time_ftv.getText());
	  sleepDevice(1);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  @Test(priority=6)
  public void TestLockDelay_30Sec() throws InterruptedException{
	  setting.settings_lock_delay.click();
	  setting.lock_delay_30_seconds.click();
	  Assert.assertEquals("30 Seconds", setting.lock_delay_time_ftv.getText());
	  sleepDevice(30);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=7)
  public void TestLockDelay_1Min() throws InterruptedException{
	  setting.settings_lock_delay.click();
	  setting.lock_delay_1_minute.click();
	  Assert.assertEquals("1 Minute", setting.lock_delay_time_ftv.getText());
	  sleepDevice(60);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=8)
  public void TestLockDelay_2Min(){
	  setting.settings_lock_delay.click();
	  setting.lock_delay_2_minute.click();
	  Assert.assertEquals("2 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(120);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=9)
  public void TestLockDelay_3Min(){
	  setting.settings_lock_delay.click();
	  setting.lock_delay_3_minutes.click();
	  Assert.assertEquals("3 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(180);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=10)
  public void TestLockDelay_4Min(){
	  setting.settings_lock_delay.click();
	  setting.lock_delay_4_minutes.click();
	  Assert.assertEquals("4 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(240);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=11)
  public void TestLockDelay_5Min(){
	  setting.settings_lock_delay.click();
	  setting.lock_delay_5_minutes.click();
	  Assert.assertEquals("5 Minutes", setting.lock_delay_time_ftv.getText());
	  sleepDevice(300);
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @Test(priority=12)
  public void TestDataSavingMode()throws Exception{
	  ae=GetElement.ByName("Data Saving Mode", myApp);
	  ae.click();
	  GetElement.ByName("OFF", myApp).click();
  }
  
 
  @Test(priority=13)
  public void TestNotifications()throws Exception{
	  ae=GetElement.ByName("Notifications", myApp);
	  ae.click();
	  GetElement.ByName("OFF", myApp).click();
  }
  
  @Test(priority=14)
  public void TestWallpaper()throws Exception{
	  ae=GetElement.ByName("Wallpaper", myApp);
	  ae.click();
	  GetElement.ByName("OFF", myApp).click();
  }
  
  @Test(priority=15)
  public void TestWeather()throws Exception{
	  setting.settings_weather.click();
	  //ae.click();
	  GetElement.ByName("OFF", myApp).click();
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  myApp=InitAppium.launchApp("com.airfind.lockscreen","com.airfind.lockscreen.activity.LauncherActivity");
	  setting = PageFactory.initElements(myApp, ALSettingsScreen.class);
  }

  @AfterMethod
  public void afterMethod() {
  }

  public void sleepDevice(int seconds){
	  //Lock Device
	  try {
		  myApp.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		  Thread.sleep(seconds*1000);
	  	 } catch (InterruptedException e) {
		// TODO Auto-generated catch block
	  		  myApp.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		e.printStackTrace();
	}
	  myApp.unlockDevice();
  }
  
  public void swipeunlockDevice(){
	  SwipeHandler.VerticalSwipeUpElement(myApp,By.id("swipeUnlock"));
  }
  
  public void swipePinUnlockDevice(){
	  SwipeHandler.VerticalSwipeUpElement(myApp,By.id("swipeUnlock"));
	  GetElement.ByName("1", myApp).click();
	  GetElement.ByName("2", myApp).click();
	  GetElement.ByName("3", myApp).click();
	  GetElement.ByName("4", myApp).click();
  }
  
  public void ALTutorial(){
	  GetElement.ByName("GET STARTED", myApp).click();
	 // GetElement.ByName("GOT IT", myApp).click();
	  
  }
}
