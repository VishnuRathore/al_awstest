package lockscreen.test;

import org.testng.annotations.Test;
import actionHandler.PerformAdvanceAction;
import appScreens.ALSettingsScreen;
import appiumConfig.InitAppium;
import elementHandler.GetElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;

public class ALDeviceTest {
	AndroidDriver myApp;
	WebElement ae;
	
  @Test
  public void ALLockInitTest() {
	  /*
	   * AWS Testing- To Verify the compatibility of AL on Top 10 Device Pool
	   */
	  sleepDevice(1);
	  PerformAdvanceAction.takeScreenshot(myApp,"AL_Lockscreen");
	  System.out.println(myApp.currentActivity());
	  Assert.assertEquals(".activity.FirstRunActivity",myApp.currentActivity());
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  myApp=InitAppium.launchApp("com.airfind.lockscreen",".activity.LockScreenActivity");
  }

  @AfterMethod
  public void afterMethod() {
  }

  public void sleepDevice(int seconds){
	  //Lock Device
	  try {
		  myApp.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
		  Thread.sleep(seconds*1000);
	  	 } catch (InterruptedException e) {
	  		  myApp.unlockDevice();
		//e.printStackTrace();
	}
	  myApp.longPressKeyCode(AndroidKeyCode.KEYCODE_POWER);
	
  }
}
